# Installation

For install and launch the project , please follow this instructions:

Clone the repository

    git clone https://gitlab.com/Zarbtinou/test-aerial-front.git

Switch to the repo folder

    cd test-aerial-front

Install all the dependencies using composer

    npm install

## Don't forget to update the backendUri variable in src/_services/cartService.js to match with your backend url

Start the local development server

    npm run dev

You can now access the server at http://localhost:8080
