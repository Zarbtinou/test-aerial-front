export const cartService = {
  buyProducts
}

const backendUri = 'http://localhost:3000'

function buyProducts (products, amount) {
  const productsIdsArray = []
  products.forEach(function (product) {
    productsIdsArray.push(product.id)
  })
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({product_ids: productsIdsArray, amount: amount})
  }
  return fetch(backendUri + '/transaction', requestOptions)
}
