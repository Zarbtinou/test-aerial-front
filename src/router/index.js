import Vue from 'vue'
import Router from 'vue-router'
import AllProducts from '../components/AllProducts'
import ProductPage from '../components/ProductPage'
import Cart from '../components/Cart'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'AllProducts',
      component: AllProducts
    },
    {
      path: '/product/:id',
      name: 'ProductPage',
      component: ProductPage
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart
    }
  ]
})
